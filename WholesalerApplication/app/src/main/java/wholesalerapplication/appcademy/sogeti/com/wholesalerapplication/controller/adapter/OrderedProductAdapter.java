package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment.ProductDetailsFragment;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.OrderItem;

/**
 * Created by viskashy on 6/20/2016.
 */
public class OrderedProductAdapter extends RecyclerView.Adapter<OrderedProductAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private int mResource;
    private List<OrderItem> mOrderItem;
    private Context mContext;
    private View view;

    public OrderedProductAdapter(Context context, int resource, List<OrderItem> productOrder) {
        mResource = resource;
        mContext = context;
        mOrderItem = productOrder;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getItemCount() {
        return mOrderItem.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product_ordered, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        OrderItem orderItem = mOrderItem.get(position);
        holder.productName.setText(orderItem.getMenuItem().getName().toString());
        String image_url = orderItem.getMenuItem().getImage();
        Picasso.with(mContext).load(image_url).into(holder.productImage);
        String price = String.valueOf(String.format("%.2f", orderItem.getMenuItem().getPrice()));
        holder.productPrice.setText(price);
        Date d = orderItem.getCreated();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String date = sdf.format(d);
        holder.orderDate.setText(date);

        holder.retailerId.setText(orderItem.getOwnerId());

    }



//    @Override
//    public int getCount() {
//        return mOrderItem.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mOrderItem.get(position);
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        view = convertView == null ? mInflater.inflate(mResource, parent, false) : convertView;
//        holder = new ViewHolder(view);
//
//        OrderItem orderItem = mOrderItem.get(position);
//
//        holder.productName.setText(orderItem.getMenuItem().getName().toString());
//        String image_url = orderItem.getMenuItem().getImage();
//        Picasso.with(mContext).load(image_url).into(holder.productImage);
//        String price = String.valueOf(String.format("%.2f", orderItem.getMenuItem().getPrice()));
//        holder.productPrice.setText(price);
//        Date d = orderItem.getCreated();
//
//        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
//        String date = sdf.format(d);
//        holder.orderDate.setText(date);
//
//        holder.retailerId.setText(orderItem.getOwnerId());
//
//        return view;
//    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.productImageView)
        ImageView productImage;
        @Bind(R.id.productName)
        TextView productName;
        @Bind(R.id.retailerId)
        TextView retailerId;
        @Bind(R.id.productPrice)
        TextView productPrice;
        @Bind(R.id.orderDate)
        TextView orderDate;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ProductDetailsFragment detailsFragment = new ProductDetailsFragment();
            Bundle bundle = new Bundle();
            int position = getAdapterPosition();
            OrderItem orderItem = mOrderItem.get(position);
            bundle.putSerializable("orderItem", orderItem);
            detailsFragment.setArguments(bundle);
            FragmentManager manager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mainContainer, detailsFragment);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}
