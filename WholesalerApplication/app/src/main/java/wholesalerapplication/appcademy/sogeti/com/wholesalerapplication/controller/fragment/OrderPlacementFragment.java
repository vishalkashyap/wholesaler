package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;

import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter.OrderPlacementAdapter;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Location;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Menu;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;


public class OrderPlacementFragment extends Fragment {


    @Bind(R.id.orderTotal)
    TextView orderTotalView;

//    @Bind(R.id.submitOrder)
//    Button submitButton;


    private Category category;
    private Location location;
    private OrderPlacementAdapter adapter;
    private   View mView;
    private RecyclerView mOrderPlacementList;
    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    public void onResume(){
        super.onResume();
        if(adapter!= null){
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mView != null) {
            return mView;
        }
        mView = inflater.inflate(R.layout.fragment_order_placement, null, false);
        ButterKnife.bind(this,mView);
        mOrderPlacementList = (RecyclerView) mView.findViewById(R.id.orderPlacementList);
        mOrderPlacementList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mOrderPlacementList.setLayoutManager(mLayoutManager);

        Bundle bundle = getArguments();
        category = (Category) bundle.getSerializable("category");
        location = (Location) bundle.getSerializable("location");

        final String title = category.getName() + " " + getString(R.string.order_text);

        getActivity().setTitle(title);

        orderTotalView.setText(String.format(getString(R.string.order_total_text), 0.00));

        final LoadingCallback<Menu> menuLoadingCallback = new LoadingCallback<Menu>(getContext(), getString(R.string.loading_menu_items), true) {
            @Override
            public void handleResponse(Menu menu) {
                adapter = new OrderPlacementAdapter(getContext(), R.layout.list_item_order_placement, menu.getMenuItem(), orderTotalView);

                mOrderPlacementList.setAdapter(adapter);

                super.handleResponse(menu);
            }
        };

        LoadingCallback<Location> locationLoadingCallback = new LoadingCallback<Location>(getContext(), getString(R.string.loading_menu_items), true) {
            @Override
            public void handleResponse(Location location) {
                Menu menu = location.getMenu();
                Backendless.Data.of(Menu.class).loadRelations(menu, Arrays.asList("menuItem"), menuLoadingCallback);

                super.handleResponse(location);
            }
        };

        Backendless.Data.of(Location.class).loadRelations(location, Arrays.asList("menu"), locationLoadingCallback);


        Button submitButton= (Button) mView.findViewById(R.id.submitOrder);
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Order order = adapter.getOrder();
                int total = (int) adapter.getOrder().calculateTotal();
                if (total != 0) {
                    OrderConfirmationFragment orderConfirmationFragment = new OrderConfirmationFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("category", category);
                    bundle.putSerializable("location", location);
                    bundle.putSerializable("order", order);
                    orderConfirmationFragment.setArguments(bundle);
                    android.support.v4.app.FragmentManager manager = getFragmentManager();
                    FragmentTransaction ft = manager.beginTransaction();
                    ft.replace(R.id.mainContainer, orderConfirmationFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                } else {
                    Toast.makeText(getContext(), getString(R.string.warning_item_is_not_selected), Toast.LENGTH_SHORT).show();

                }
            }

        });

        return mView;
    }

}
