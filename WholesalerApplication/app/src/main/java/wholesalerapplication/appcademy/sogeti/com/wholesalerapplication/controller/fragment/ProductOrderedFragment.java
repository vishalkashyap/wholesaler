package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter.OrderedProductAdapter;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.OrderItem;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;


public class ProductOrderedFragment extends Fragment {
    private BackendlessCollection<OrderItem> orderItem;
    private List<OrderItem> totalOrderItem = new ArrayList<>();
    private boolean isLoadingItems = false;
    private OrderedProductAdapter adapter;
    private Order order;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mProductOrderedList;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mView != null) {
            return mView;
        }

        mView = inflater.inflate(R.layout.fragment_product_ordered, null, false);
        mProductOrderedList = (RecyclerView) mView.findViewById(R.id.productOrderList);
        mProductOrderedList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mProductOrderedList.setLayoutManager(mLayoutManager);
        adapter = new OrderedProductAdapter(getActivity(), R.layout.list_product_ordered, totalOrderItem);
        mProductOrderedList.setAdapter(adapter);


        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setRelated(Arrays.asList("menuItem"));

        BackendlessDataQuery query = new BackendlessDataQuery(queryOptions);

        Backendless.Data.of(OrderItem.class).find(query, new LoadingCallback<BackendlessCollection<OrderItem>>(getContext(), getString(R.string.loading_product_order), true) {
            @Override
            public void handleResponse(BackendlessCollection<OrderItem> restaurantsBackendlessCollection) {
                orderItem = restaurantsBackendlessCollection;

                addMoreItems(restaurantsBackendlessCollection);

                super.handleResponse(restaurantsBackendlessCollection);
            }
        });


//        mProductOrderedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ProductDetailsFragment detailsFragment = new ProductDetailsFragment();
//                Bundle bundle = new Bundle();
//                OrderItem orderItem = totalOrderItem.get(position);
//                bundle.putSerializable("orderItem", orderItem);
//                detailsFragment.setArguments(bundle);
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.mainContainer, detailsFragment);
//                ft.addToBackStack(null);
//                ft.commit();
//            }
//        });

//        mProductOrderedList.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (needToLoadItems(firstVisibleItem, visibleItemCount, totalItemCount)) {
//                    isLoadingItems = true;
//
//                    orderItem.nextPage(new LoadingCallback<BackendlessCollection<OrderItem>>(getContext()) {
//                        @Override
//                        public void handleResponse(BackendlessCollection<OrderItem> nextPage) {
//                            orderItem = nextPage;
//
//                            addMoreItems(nextPage);
//
//                            isLoadingItems = false;
//                        }
//                    });
//                }
//            }
//        });


        return mView;
    }


    private boolean needToLoadItems(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        return !isLoadingItems && totalItemCount != 0 && totalItemCount - (visibleItemCount + firstVisibleItem) < visibleItemCount / 2;
    }


    private void addMoreItems(BackendlessCollection<OrderItem> nextPage) {
        totalOrderItem.addAll(nextPage.getCurrentPage());
        adapter.notifyDataSetChanged();
    }


}
