package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.OrderItem;


public class ProductDetailsFragment extends Fragment {

    @Bind(R.id.productName)
    TextView productName;

    @Bind(R.id.productImageView)
    ImageView product_image;

    @Bind(R.id.retailerName_result)
    TextView retailerName;

    @Bind(R.id.priceResult)
    TextView productPrice;

    @Bind(R.id.orderDateResult)
    TextView orderDate;

    @Bind(R.id.quantityResult)
    TextView quantity;

    @Bind(R.id.descriptionResult)
    TextView description;

    private OrderItem orderItem;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_product_details, null, false);
        ButterKnife.bind(this, mView);

        Bundle bundle = getArguments();

        orderItem = (OrderItem) bundle.getSerializable("orderItem");
        productName.setText(orderItem.getMenuItem().getName());

        String image_url = orderItem.getMenuItem().getImage();
        Picasso.with(getContext()).load(image_url).into(product_image);


        retailerName.setText(orderItem.getOwnerId());

        String price = String.valueOf(String.format("%.2f", orderItem.getMenuItem().getPrice()));
        productPrice.setText(price);
        Date d = orderItem.getCreated();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        String date = sdf.format(d);
        orderDate.setText(date);

        quantity.setText(String.valueOf(orderItem.getQuantity()));

        description.setText(String.format(orderItem.getMenuItem().getDescription()));
        return mView;
    }

}
