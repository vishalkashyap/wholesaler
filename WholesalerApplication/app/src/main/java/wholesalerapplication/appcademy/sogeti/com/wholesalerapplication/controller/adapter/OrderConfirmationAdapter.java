package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.OrderItem;

/**
 * Created by viskashy on 6/15/2016.
 */
public class OrderConfirmationAdapter extends RecyclerView.Adapter<OrderConfirmationAdapter.ViewHolder>{

    private LayoutInflater mInflater;
    private int mResource;
    private Order order;
    private TextView totalPriceView;
    private List<OrderItem> mOrderItem;
    private Context mContext;




    public OrderConfirmationAdapter(Context context, int resource, List<OrderItem> menuItems, Order order,
                                    TextView totalPriceView) {
        mResource = resource;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.order = order;
        this.totalPriceView = totalPriceView;
        mOrderItem=menuItems;
        mContext=context;
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//
//        final OrderItem orderItem = getItem(position);
//
//        if (convertView == null) {
//
//            convertView = mInflater.inflate(mResource, parent, false);
//            holder = new ViewHolder(convertView);
//
//            holder.textWatcher = new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    if (s.length() != 0) {
//                        Integer newQuantity = Integer.valueOf(s.toString());
//                        orderItem.setQuantity(newQuantity);
//                        holder.priceTextView.setText(String.valueOf(orderItem.getMenuItem().getPrice() * orderItem.getQuantity()));
//                        totalPriceView.setText(String.format(getContext().getString(R.string.order_total_text), order.calculateTotal()));
//                    }
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            };
//
//            convertView.setTag(holder);
//        } else {
//      /* We recycle a View that already exists */
//            holder = (ViewHolder) convertView.getTag();
//            holder.cancelButton.setOnClickListener(null);
//            holder.quantityEditText.removeTextChangedListener(holder.textWatcher);
//        }
//
//        holder.quantityEditText.setText(String.valueOf(orderItem.getQuantity()));
//        holder.quantityEditText.addTextChangedListener(holder.textWatcher);
//
//        holder.menuItemTextView.setText(orderItem.getMenuItem().getName());
//
//        holder.cancelButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                order.removeItem(orderItem);
//                recalculateTotalPrice();
//                remove(orderItem);
//                notifyDataSetChanged();
//            }
//        });
//
//        holder.priceTextView.setText(String.valueOf(orderItem.getMenuItem().getPrice() * orderItem.getQuantity()));
//
//        recalculateTotalPrice();
//
//        return convertView;
//    }

    private void recalculateTotalPrice() {
        totalPriceView.setText(String.format(mContext.getString(R.string.total_text), order.calculateTotal()));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_order_confirmation, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final OrderItem orderItem = mOrderItem.get(position);


        holder.textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    Integer newQuantity = Integer.valueOf(s.toString());
                    orderItem.setQuantity(newQuantity);
                    holder.priceTextView.setText(String.valueOf(orderItem.getMenuItem().getPrice() * orderItem.getQuantity()));
                    totalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        holder.quantityEditText.setText(String.valueOf(orderItem.getQuantity()));
        holder.quantityEditText.addTextChangedListener(holder.textWatcher);

        holder.menuItemTextView.setText(orderItem.getMenuItem().getName());

        holder.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                order.removeItem(orderItem);
                recalculateTotalPrice();
               // remove(orderItem);
              //  orderItem.remove(orderItem);

                notifyDataSetChanged();
            }
        });

        holder.priceTextView.setText(String.valueOf(orderItem.getMenuItem().getPrice() * orderItem.getQuantity()));

        recalculateTotalPrice();

    }

    @Override
    public int getItemCount() {
        return mOrderItem.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.menuItem)
        TextView menuItemTextView;
        @Bind(R.id.quantity)
        EditText quantityEditText;
        @Bind(R.id.itemPrice)
        TextView priceTextView;
        TextWatcher textWatcher;
        @Bind(R.id.cancelButton)
        ImageButton cancelButton;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}
