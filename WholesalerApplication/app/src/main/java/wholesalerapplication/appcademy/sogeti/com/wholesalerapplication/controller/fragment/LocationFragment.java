package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter.LocationAdapter;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;


public class LocationFragment extends Fragment {

    private Category category;
    private LocationAdapter adapter;
    private RecyclerView mLocationList;
    private RecyclerView.LayoutManager mLayoutManager;


    @BindString(R.string.locations_text)
    String title;

    @Bind(R.id.titleLocation)
    TextView screenTitle;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = getActivity().getLayoutInflater().inflate(R.layout.fragment_location, null);
        ButterKnife.bind(this,mView);

        mLocationList = (RecyclerView) mView.findViewById(R.id.locationList);
        mLocationList.setHasFixedSize(true);
        mLayoutManager=new LinearLayoutManager(getContext());
        mLocationList.setLayoutManager(mLayoutManager);


        Bundle bundle = getArguments();
        category = (Category) bundle.getSerializable("category");
        title = category.getName() + " " + getString(R.string.locations_text);
        getActivity().setTitle(title);

        screenTitle.setText(title);

        adapter = new LocationAdapter(getActivity(), R.layout.list_item_location, category.getLocations(),category);

        mLocationList.setAdapter(adapter);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                OrderPlacementFragment orderPlacementFragment = new OrderPlacementFragment();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("category", category);
//                bundle.putSerializable("location", category.getLocations().get(position));
//                orderPlacementFragment.setArguments(bundle);
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.mainContainer, orderPlacementFragment);
//                ft.addToBackStack(null);
//
//                ft.commit();
//            }
//        });
        return mView;
    }
}
