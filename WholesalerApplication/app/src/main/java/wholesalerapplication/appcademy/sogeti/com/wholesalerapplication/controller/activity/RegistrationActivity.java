package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.BackendSettings;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.Validator;

/**
 * Created by viskashy on 6/2/2016.
 */
public class RegistrationActivity extends Activity {

    @Bind(R.id.nameField)
    EditText nameField;

    @Bind(R.id.emailField)
    EditText emailField;

    @Bind(R.id.passwordField)
    EditText passwordField;

    @Bind(R.id.passwordConfirmField)
    EditText passwordConfirmField;

    @Bind(R.id.registerButton)
    Button registerButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Backendless.initApp(this, BackendSettings.APPLICATION_ID, BackendSettings.ANDROID_SECRET_KEY, BackendSettings.VERSION);
        ButterKnife.bind(this);
        View.OnClickListener registerButtonClickListener = createRegisterButtonClickListener();

        registerButton.setOnClickListener(registerButtonClickListener);
    }

    public boolean isRegistrationValuesValid(CharSequence name, CharSequence email, CharSequence password,
                                             CharSequence passwordConfirm) {
        return Validator.isNameValid(this, name)
                && Validator.isEmailValid(this, email)
                && Validator.isPasswordValid(this, password)
                && isPasswordsMatch(password, passwordConfirm);
    }

    public boolean isPasswordsMatch(CharSequence password, CharSequence passwordConfirm) {
        if (!TextUtils.equals(password, passwordConfirm)) {
            Toast.makeText(this, getString(R.string.warning_passwords_do_not_match), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public void registerUser(String name, String email, String password,
                             AsyncCallback<BackendlessUser> registrationCallback) {
        Log.d("RegitrationActivity", "name : " + name + " email : " + email +"password : "+password+ " registrationCallback : " + registrationCallback);
        BackendlessUser user = new BackendlessUser();
        user.setEmail(email);
        user.setPassword(password);
        user.setProperty("name", name);

        Backendless.UserService.register(user, registrationCallback);
    }

    public LoadingCallback<BackendlessUser> createRegistrationCallback()
    {
        return new LoadingCallback<BackendlessUser>( this, getString( R.string.loading_register ) )
        {
            @Override
            public void handleResponse( BackendlessUser registeredUser )
            {
                getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
                super.handleResponse( registeredUser );
                Intent registrationResult = new Intent();
                registrationResult.putExtra( BackendlessUser.EMAIL_KEY, registeredUser.getEmail() );
                registrationResult.putExtra( BackendlessUser.PASSWORD_KEY, registeredUser.getPassword() );
                setResult( RESULT_OK, registrationResult );
                RegistrationActivity.this.finish();
            }
        };
    }

    public View.OnClickListener createRegisterButtonClickListener()
    {
        return new View.OnClickListener()
        {
            @Override
            public void onClick( View v )
            {
                CharSequence name = nameField.getText();
                CharSequence email = emailField.getText();
                CharSequence password = passwordField.getText();
                CharSequence passwordConfirmation = passwordConfirmField.getText();

                if( isRegistrationValuesValid( name, email, password, passwordConfirmation ) )
                {
                    LoadingCallback<BackendlessUser> registrationCallback = createRegistrationCallback();

                    registrationCallback.showLoading();
                    registerUser( name.toString(), email.toString(), password.toString(), registrationCallback );
                }
            }
        };
    }
}
