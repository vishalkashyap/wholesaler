package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication;

import android.app.Application;

import com.backendless.Backendless;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.BackendSettings;

/**
 * Created by viskashy on 7/12/2016.
 */
public class ApplicationEX extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Backendless.initApp(this, BackendSettings.APPLICATION_ID, BackendSettings.ANDROID_SECRET_KEY, BackendSettings.VERSION);

    }
}
