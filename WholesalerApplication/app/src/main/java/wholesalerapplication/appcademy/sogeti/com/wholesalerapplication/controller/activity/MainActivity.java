package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment.CategoryFragment;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment.ProductOrderedFragment;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.constant.Constants;

public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Constants.admin = (String) getIntent().getSerializableExtra("admin");

        if (Constants.admin.equals(Constants.admin_ID)) {
            ProductOrderedFragment productOrderedFragment = new ProductOrderedFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.mainContainer, productOrderedFragment);
            //ft.addToBackStack(null);
            ft.commit();

        } else {
            CategoryFragment categoryFragment = new CategoryFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.mainContainer, categoryFragment);
            //ft.addToBackStack(null);
            ft.commit();
        }
    }

}
