package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;

/**
 * Created by viskashy on 6/10/2016.
 */
public class SplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(1500);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    }

