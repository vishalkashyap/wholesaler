package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter.OrderConfirmationAdapter;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Location;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.DialogHelper;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;


public class OrderConfirmationFragment extends Fragment {


    @BindString(R.string.order_confirmation_title_text)
    String title;

//    @Bind(R.id.titleConfirmation)
//    TextView screenTitle;

    //@Bind(R.id.footerPrice)
    TextView totalPriceView;

    // @Bind(R.id.submit)
    Button submitOrderButton;

    private Category category;
    private Location location;
    private Order order;
    private OrderConfirmationAdapter adapter;
    private RecyclerView orderConfirmationList;
    private View footerView;
    private View mView;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mView != null) {
            return mView;
        }
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_order_confirmation, null, false);
        ButterKnife.bind(getActivity(), mView);

        orderConfirmationList = (RecyclerView) mView.findViewById(R.id.orderConfirmationList);
        orderConfirmationList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        orderConfirmationList.setLayoutManager(mLayoutManager);

        //added by snehal
        submitOrderButton = (Button) mView.findViewById(R.id.submit);
        totalPriceView = (TextView) mView.findViewById(R.id.footerPrice);

        Bundle bundle = getArguments();
        category = (Category) bundle.getSerializable("category");
        location = (Location) bundle.getSerializable("location");
        order = (Order) bundle.getSerializable("order");

        title = category.getName() + " " + getString(R.string.order_confirmation_title_text);
        getActivity().setTitle(title);

        TextView screenTitle = (TextView) mView.findViewById(R.id.titleConfirmation);
        screenTitle.setText(title);


        //commented by snehal
       /* footerView = inflater.inflate(R.layout.list_footer_order_confirmation, null);
        //orderConfirmationList.addFooterView(footerView);
        orderConfirmationList.addView(footerView,0);*/


        adapter = new OrderConfirmationAdapter(getContext(), R.layout.list_item_order_confirmation, order.getItems(), order, totalPriceView);
        orderConfirmationList.setAdapter(adapter);

        submitOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderedSubmit();
            }
        });

        return mView;
    }

    private void orderedSubmit() {
        int total = (int) order.calculateTotal();
        if (total != 0) {
            final ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage(getString(R.string.loading_submit_order));
            progressDialog.show();

            Backendless.Counters.incrementAndGet("order_number", new AsyncCallback<Integer>() {
                @Override
                public void handleResponse(Integer response) {
                    progressDialog.dismiss();

                    order.setNumber(response);

                    // save Order on backend
                    Backendless.Data.of(Order.class).save(order, new LoadingCallback<Order>(getContext(), getString(R.string.loading_placing_order), true) {
                        @Override
                        public void handleResponse(Order response) {
                            super.handleResponse(response);
                            OrderSuccessFragment orderSuccessFragment = new OrderSuccessFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("category", category);
                            bundle.putSerializable("location", location);
                            bundle.putSerializable("order", order);
                            orderSuccessFragment.setArguments(bundle);
                            android.support.v4.app.FragmentManager manager = getFragmentManager();
                            FragmentTransaction ft = manager.beginTransaction();
                            ft.replace(R.id.mainContainer, orderSuccessFragment);
                            ft.addToBackStack(null);

                            ft.commit();

                        }
                    });
                }

                @Override
                public void handleFault(BackendlessFault fault) {
                    progressDialog.dismiss();

                    DialogHelper.createErrorDialog(getContext(), "BackendlessFault", fault.getMessage()).show();
                }
            });
        } else {
            Toast.makeText(getContext(), getString(R.string.warning_item_is_not_selected), Toast.LENGTH_SHORT).show();

        }
    }
}
