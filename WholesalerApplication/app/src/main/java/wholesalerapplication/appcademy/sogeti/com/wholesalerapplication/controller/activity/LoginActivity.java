package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.Validator;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.constant.Constants;

/**
 * Created by viskashy on 6/2/2016.
 */
public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.loginButton)
    Button loginButton;

    @Bind(R.id.registerPromptText)
    TextView registerPromptView;

    @Bind(R.id.emailField)
    EditText emailField;

    @Bind(R.id.passwordField)
    EditText passwordField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        loginButton.setOnClickListener(createLoginButtonListener());

        makeRegistrationLink();
    }

    public void makeRegistrationLink() {
        SpannableString registrationPrompt = new SpannableString(getString(R.string.register_prompt));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startRegistrationActivity();
            }
        };

        String linkText = getString(R.string.register_link);
        int linkStartIndex = registrationPrompt.toString().indexOf(linkText);
        int linkEndIndex = linkStartIndex + linkText.length();
        registrationPrompt.setSpan(clickableSpan, linkStartIndex, linkEndIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        registerPromptView.setText(registrationPrompt);
        registerPromptView.setMovementMethod(LinkMovementMethod.getInstance());
    }


    public void startRegistrationActivity() {
        Intent registrationIntent = new Intent(this, RegistrationActivity.class);
        startActivityForResult(registrationIntent, Constants.REGISTER_REQUEST_CODE);
    }

    public View.OnClickListener createLoginButtonListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CharSequence email = emailField.getText();
                CharSequence password = passwordField.getText();

                if (isLoginValuesValid(email, password)) {
                    LoadingCallback<BackendlessUser> loginCallback = createLoginCallback();

                    loginCallback.showLoading();
                    loginUser(email.toString(), password.toString(), loginCallback);
                }
            }
        };
    }

    public boolean isLoginValuesValid(CharSequence email, CharSequence password) {
        return Validator.isEmailValid(this, email) && Validator.isPasswordValid(this, password);
    }

    public void loginUser(String email, String password, AsyncCallback<BackendlessUser> loginCallback) {
        Backendless.UserService.login(email, password, loginCallback);
    }


    public LoadingCallback<BackendlessUser> createLoginCallback() {
        return new LoadingCallback<BackendlessUser>(this, getString(R.string.loading_login)) {
            @Override
            public void handleResponse(BackendlessUser loggedInUser) {
                super.handleResponse(loggedInUser);
                Constants.admin = loggedInUser.getObjectId();
                Intent productListingIntent = new Intent(LoginActivity.this, MainActivity.class);
                productListingIntent.putExtra("admin", Constants.admin);
                startActivity(productListingIntent);
                //finish();
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REGISTER_REQUEST_CODE:
                    String email = data.getStringExtra(BackendlessUser.EMAIL_KEY);
                    String password = data.getStringExtra(BackendlessUser.PASSWORD_KEY);
                    EditText emailField = (EditText) findViewById(R.id.emailField);
                    emailField.setText(email);

                    EditText passwordField = (EditText) findViewById(R.id.passwordField);
                    passwordField.setText(password);

                    Toast.makeText(this, getString(R.string.info_registered_success), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
