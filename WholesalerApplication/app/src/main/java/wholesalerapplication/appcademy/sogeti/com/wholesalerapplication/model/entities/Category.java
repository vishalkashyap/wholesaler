package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.persistence.BackendlessDataQuery;

import java.io.Serializable;

public class Category implements Serializable {
    private String name;
    private String ownerId;
    private String objectId;
    private String cuisine;
    private java.util.Date updated;
    private java.util.Date created;
    private java.util.List<Location> locations;
    private BackendlessUser owner;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getObjectId() {
        return objectId;
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    public java.util.Date getUpdated() {
        return updated;
    }

    public java.util.Date getCreated() {
        return created;
    }

    public java.util.List<Location> getLocations() {
        return locations;
    }

    public void setLocations(java.util.List<Location> locations) {
        this.locations = locations;
    }

    public BackendlessUser getOwner() {
        return owner;
    }

    public void setOwner(BackendlessUser owner) {
        this.owner = owner;
    }


    public Category save() {
        return Backendless.Data.of(Category.class).save(this);
    }

    public Future<Category> saveAsync() {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<Category> future = new Future<Category>();
            Backendless.Data.of(Category.class).save(this, future);

            return future;
        }
    }

    public void saveAsync(AsyncCallback<Category> callback) {
        Backendless.Data.of(Category.class).save(this, callback);
    }

    public Long remove() {
        return Backendless.Data.of(Category.class).remove(this);
    }

    public Future<Long> removeAsync() {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<Long> future = new Future<Long>();
            Backendless.Data.of(Category.class).remove(this, future);

            return future;
        }
    }

    public void removeAsync(AsyncCallback<Long> callback) {
        Backendless.Data.of(Category.class).remove(this, callback);
    }

    public static Category findById(String id) {
        return Backendless.Data.of(Category.class).findById(id);
    }

    public static Future<Category> findByIdAsync(String id) {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<Category> future = new Future<Category>();
            Backendless.Data.of(Category.class).findById(id, future);

            return future;
        }
    }

    public static void findByIdAsync(String id, AsyncCallback<Category> callback) {
        Backendless.Data.of(Category.class).findById(id, callback);
    }

    public static Category findFirst() {
        return Backendless.Data.of(Category.class).findFirst();
    }

    public static Future<Category> findFirstAsync() {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<Category> future = new Future<Category>();
            Backendless.Data.of(Category.class).findFirst(future);

            return future;
        }
    }

    public static void findFirstAsync(AsyncCallback<Category> callback) {
        Backendless.Data.of(Category.class).findFirst(callback);
    }

    public static Category findLast() {
        return Backendless.Data.of(Category.class).findLast();
    }

    public static Future<Category> findLastAsync() {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<Category> future = new Future<Category>();
            Backendless.Data.of(Category.class).findLast(future);

            return future;
        }
    }

    public static void findLastAsync(AsyncCallback<Category> callback) {
        Backendless.Data.of(Category.class).findLast(callback);
    }

    public static BackendlessCollection<Category> find(BackendlessDataQuery query) {
        return Backendless.Data.of(Category.class).find(query);
    }

    public static Future<BackendlessCollection<Category>> findAsync(BackendlessDataQuery query) {
        if (Backendless.isAndroid()) {
            throw new UnsupportedOperationException("Using this method is restricted in Android");
        } else {
            Future<BackendlessCollection<Category>> future = new Future<BackendlessCollection<Category>>();
            Backendless.Data.of(Category.class).find(query, future);

            return future;
        }
    }

    public static void findAsync(BackendlessDataQuery query, AsyncCallback<BackendlessCollection<Category>> callback) {
        Backendless.Data.of(Category.class).find(query, callback);
    }


}