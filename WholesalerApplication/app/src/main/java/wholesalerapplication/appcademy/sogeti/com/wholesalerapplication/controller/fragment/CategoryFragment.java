package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter.CategoryAdapter;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility.LoadingCallback;

public class CategoryFragment extends Fragment{

    private BackendlessCollection<Category> category;
    private List<Category> totalCategory = new ArrayList<>();
    private boolean isLoadingItems = false;
    private RecyclerView.LayoutManager mLayoutManager;
    private CategoryAdapter adapter;
    // private ListView mCategoryList;
    private RecyclerView mCategoryList;
    private View mView;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mView != null) {
            return mView;
        }

        mView = inflater.inflate(R.layout.fragment_category, null, false);
        //mCategoryList = (ListView) mView.findViewById(R.id.list);
        mCategoryList = (RecyclerView) mView.findViewById(R.id.categoryList);
        mCategoryList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mCategoryList.setLayoutManager(mLayoutManager);
        adapter = new CategoryAdapter(getActivity(), R.layout.list_item_category, totalCategory);
        mCategoryList.setAdapter(adapter);


        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setRelated(Arrays.asList("locations"));

        BackendlessDataQuery query = new BackendlessDataQuery(queryOptions);

        Backendless.Data.of(Category.class).find(query, new LoadingCallback<BackendlessCollection<Category>>(getContext(), getString(R.string.loading_restaurants), true) {
            @Override
            public void handleResponse(BackendlessCollection<Category> categoryBackendlessCollection) {
                category = categoryBackendlessCollection;

                addMoreItems(categoryBackendlessCollection);

                super.handleResponse(categoryBackendlessCollection);
            }
        });

//        mCategoryList.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                LocationFragment locationFragment = new LocationFragment();
//                Bundle bundle = new Bundle();
//                int itemPosition = mCategoryList.indexOfChild(v);
//                bundle.putSerializable("category", totalCategory.get(itemPosition));
//                locationFragment.setArguments(bundle);
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.mainContainer, locationFragment);
//                ft.addToBackStack(null);
//                ft.commit();
//
//            }
//        });


//        adapter = new CategoryAdapter(getActivity(), R.layout.list_item_category, totalCategory);
//        mCategoryList.setAdapter(adapter);
//        mCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                LocationFragment locationFragment = new LocationFragment();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("category", totalCategory.get(position));
//                locationFragment.setArguments(bundle);
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.mainContainer, locationFragment);
//                ft.addToBackStack(null);
//                ft.commit();
//
//            }
//        });
//
//
//        mCategoryList.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (needToLoadItems(firstVisibleItem, visibleItemCount, totalItemCount)) {
//                    isLoadingItems = true;
//
//                    category.nextPage(new LoadingCallback<BackendlessCollection<Category>>(getContext()) {
//                        @Override
//                        public void handleResponse(BackendlessCollection<wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category> nextPage) {
//                            category = nextPage;
//
//                            addMoreItems(nextPage);
//
//                            isLoadingItems = false;
//                        }
//                    });
//                }
//            }
//        });


        return mView;
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//
//        adapter.setOnItemClickListener(new CategoryAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
//                LocationFragment locationFragment = new LocationFragment();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("category", totalCategory.get(position));
//                locationFragment.setArguments(bundle);
//                FragmentManager manager = getFragmentManager();
//                FragmentTransaction ft = manager.beginTransaction();
//                ft.replace(R.id.mainContainer, locationFragment);
//                ft.addToBackStack(null);
//                ft.commit();
//
//            }
//        });
//
//
//    }

    private boolean needToLoadItems(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        return !isLoadingItems && totalItemCount != 0 && totalItemCount - (visibleItemCount + firstVisibleItem) < visibleItemCount / 2;
    }


    private void addMoreItems(BackendlessCollection<wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category> nextPage) {
        totalCategory.addAll(nextPage.getCurrentPage());
        adapter.notifyDataSetChanged();
    }

//    @Override
//    public void onItemClick(View view, int position) {
//
//        LocationFragment locationFragment = new LocationFragment();
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("category", totalCategory.get(position));
//        locationFragment.setArguments(bundle);
//        FragmentManager manager = getFragmentManager();
//        FragmentTransaction ft = manager.beginTransaction();
//        ft.replace(R.id.mainContainer, locationFragment);
//        ft.addToBackStack(null);
//        ft.commit();
//
//
//    }


}