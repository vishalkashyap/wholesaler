package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.MenuItem;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.OrderItem;

/**
 * Created by viskashy on 6/13/2016.
 */
public class OrderPlacementAdapter extends RecyclerView.Adapter<OrderPlacementAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private int mResource;
    private Order order;
    private TextView mTotalPriceView;
    private Context mContext;
    private List<MenuItem> mMenuItem;
    final String[] qtyValues = {"0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
    ViewHolder holder;
    View view;
    final String TAG = OrderPlacementAdapter.class.getSimpleName();

    public OrderPlacementAdapter(Context context, int resource, List<MenuItem> menuItems, TextView totalPriceView) {
        mContext = context;
        mResource = resource;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        order = new Order();
        mTotalPriceView = totalPriceView;
        mMenuItem = menuItems;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_order_placement, parent, false);
        holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MenuItem menuItem = mMenuItem.get(position);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, qtyValues);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.quantitySpinner.setAdapter(spinnerAdapter);

        holder.quantitySpinner.setSelection((order.getQuantity(menuItem)));

        holder.quantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int index, long id) {
                Log.d(TAG, "onItemSelected() called------------------ " + index);
                if (holder.menuItemCheckBox.isChecked()) {
                    Integer quantity = index;
                    OrderItem orderItem = order.getOrderItem(menuItem);
                    if (orderItem != null)
                        orderItem.setQuantity(quantity);
                    mTotalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.menuItemCheckBox.setChecked(order.containsMenuItem(menuItem));
        holder.menuItemCheckBox.setText(menuItem.getName());

        holder.menuItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(TAG, "onCheckedChanged---------");
                if (isChecked) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setMenuItem(menuItem);
                    if (holder.quantitySpinner.getSelectedItem().toString().equals("0")) {
                        // immediately add one item of checked menu item if it wasn't present in the order yet
                        orderItem.setQuantity(1);
                    } else {
                        orderItem.setQuantity(Integer.valueOf(holder.quantitySpinner.getSelectedItem().toString()));
                    }

                    order.getItems().add(orderItem);

                    holder.quantitySpinner.setSelection(orderItem.getQuantity());
                } else //not checked
                {
                    order.removeItem(menuItem);
                }

                mTotalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
            }
        });

        // format price
        String price = String.valueOf(String.format("%.2f", menuItem.getPrice()));
        holder.priceTextView.setText(price);

        String image_Url = menuItem.getImage();
        Picasso.with(mContext).load(image_Url).into(holder.productImage);

    }



   /* @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final MenuItem menuItem = mMenuItem.get(position);
        holder.textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (holder.menuItemCheckBox.isChecked() && s.length() != 0) {
                    Integer quantity = Integer.valueOf(s.toString());
                    OrderItem orderItem = order.getOrderItem(menuItem);
                    if (orderItem != null)
                        orderItem.setQuantity(quantity);
                    mTotalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        //modified
        holder.quantityEditText.setText(String.valueOf(order.getQuantity(menuItem)));
          holder.quantityEditText.addTextChangedListener(holder.textWatcher);




        holder.menuItemCheckBox.setChecked(order.containsMenuItem(menuItem));
        holder.menuItemCheckBox.setText(menuItem.getName());

        holder.menuItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setMenuItem(menuItem);
                    if (holder.quantityEditText.getText().toString().equals("0")) {
                        // immediately add one item of checked menu item if it wasn't present in the order yet
                        orderItem.setQuantity(1);
                    } else {
                        orderItem.setQuantity(Integer.valueOf(holder.quantityEditText.getText().toString()));
                    }

                    order.getItems().add(orderItem);

                    holder.quantityEditText.setText(orderItem.getQuantity().toString());
                } else //not checked
                {
                    order.removeItem(menuItem);
                }

                mTotalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
            }
        });

        // format price
        String price = String.valueOf(String.format("%.2f", menuItem.getPrice()));
        holder.priceTextView.setText(price);

        String image_Url = menuItem.getImage();
        Picasso.with(mContext).load(image_Url).into(holder.productImage);

    }*/


    @Override
    public int getItemCount() {
        return mMenuItem.size();
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//
//        final MenuItem menuItem = mMenuItem.get(position);
//
//        if (convertView == null) {
//
//            convertView = mInflater.inflate(mResource, parent, false);
//            holder = new ViewHolder(convertView);
//            holder.textWatcher = new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//                    if (holder.menuItemCheckBox.isChecked() && s.length() != 0) {
//                        Integer quantity = Integer.valueOf(s.toString());
//                        OrderItem orderItem = order.getOrderItem(menuItem);
//                        orderItem.setQuantity(quantity);
//                        totalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
//                    }
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            }
//            ;
//
//            convertView.setTag(holder);
//        } else {
//
//            holder = (ViewHolder) convertView.getTag();
//            holder.menuItemCheckBox.setOnCheckedChangeListener(null);
//            holder.quantityEditText.removeTextChangedListener(holder.textWatcher);
//        }
//
//        holder.quantityEditText.setText(String.valueOf(order.getQuantity(menuItem)));
//
//        holder.quantityEditText.addTextChangedListener(holder.textWatcher);
//
//        holder.menuItemCheckBox.setChecked(order.containsMenuItem(menuItem));
//        holder.menuItemCheckBox.setText(menuItem.getName());
//
//        holder.menuItemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    OrderItem orderItem = new OrderItem();
//                    orderItem.setMenuItem(menuItem);
//
//                    if (holder.quantityEditText.getText().toString().equals("0")) {
//                        // immediately add one item of checked menu item if it wasn't present in the order yet
//                        orderItem.setQuantity(1);
//                    } else {
//                        orderItem.setQuantity(Integer.valueOf(holder.quantityEditText.getText().toString()));
//                    }
//
//                    order.getItems().add(orderItem);
//
//                    holder.quantityEditText.setText(orderItem.getQuantity().toString());
//                } else //not checked
//                {
//                    order.removeItem(menuItem);
//                }
//
//                totalPriceView.setText(String.format(mContext.getString(R.string.order_total_text), order.calculateTotal()));
//            }
//        });
//
//        // format price
//        String price = String.valueOf(String.format("%.2f", menuItem.getPrice()));
//        holder.priceTextView.setText(price);
//
//        String image_Url = menuItem.getImage();
//        Picasso.with(mContext).load(image_Url).into(holder.productImage);
//
//
//        return convertView;
//    }

    public Order getOrder() {
        return order;
    }

    @Override
    public int getItemViewType(int position) {

        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.menuItemCheckBox)
        CheckBox menuItemCheckBox;
        /* @Bind(R.id.quantity)
         EditText quantityEditText;*/
        //modified
        @Bind(R.id.quantitySpinner)
        Spinner quantitySpinner;
        @Bind(R.id.itemPrice)
        TextView priceTextView;
        TextWatcher textWatcher;
        @Bind(R.id.productImage)
        ImageView productImage;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
