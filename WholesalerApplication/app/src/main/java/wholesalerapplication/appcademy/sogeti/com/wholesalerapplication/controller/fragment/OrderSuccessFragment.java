package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Location;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Order;


public class OrderSuccessFragment extends Fragment {

    @Bind(R.id.title)
    TextView screenTitle;

    @Bind(R.id.orderNumber)
    TextView orderNumberView;

    @Bind(R.id.address)
    TextView addressView;

    @Bind(R.id.newOrderButton)
    Button makeOrderButton;



    private Category category;
    private Location location;
    private Order order;
    Context context;
    OrderSuccessFragment successFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = getActivity().getLayoutInflater().inflate(R.layout.fragment_order_success, null);
        ButterKnife.bind(this, mView);




        Bundle bundle = getArguments();
        category = (Category) bundle.getSerializable("category");
        location = (Location) bundle.getSerializable("location");
        order = (Order) bundle.getSerializable("order");

        String title = category.getName() + " " + getString(R.string.order_confirmation_title_text);
        getActivity().setTitle(title);

        screenTitle.setText(title);

        orderNumberView.setText(String.format(getString(R.string.order_number), order.getNumber()));

        addressView.setText(String.format(getString(R.string.pickup_address_text), category.getName(), location.getCity(), location.getStreetAddress(), location.getPhoneNumber()));

        makeOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                closefragment();
                CategoryFragment categoryFragment = new CategoryFragment();
                android.support.v4.app.FragmentManager manager = getFragmentManager();
                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.mainContainer, categoryFragment);
                ft.addToBackStack(null);
                ft.commit();


            }
        });
        return mView;
    }

    private void closefragment() {
        getActivity().getFragmentManager().popBackStack();

    }


}
