package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.utility;

import android.content.Context;
import android.util.Patterns;
import android.widget.Toast;

import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;


public class Validator {

    public static boolean isNameValid(Context currentContext, CharSequence name) {
        if (name.toString().isEmpty()) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_name_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (!Character.isUpperCase(name.charAt(0))) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_name_lowercase), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }


    public static boolean isEmailValid(Context currentContext, CharSequence email) {
        if (email.toString().isEmpty()) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_email_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_email_invalid), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public static boolean isPasswordValid(Context currentContext, CharSequence password) {
        if (password.toString().isEmpty()) {
            Toast.makeText(currentContext, currentContext.getString(R.string.warning_password_empty), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
