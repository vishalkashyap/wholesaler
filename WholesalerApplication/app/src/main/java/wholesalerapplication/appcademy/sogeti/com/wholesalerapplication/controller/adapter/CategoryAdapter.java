package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment.LocationFragment;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private int mResource;
    private List<Category> mCategories;
    private Context mcontext;

    public CategoryAdapter(Context context, int resource, List<Category> categories) {
        mcontext = context;
        mResource = resource;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCategories = categories;

    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Category item = mCategories.get(position);
        holder.categoryNameView.setText(item.getName());
        holder.cuisineView.setText(item.getCuisine());
        String locationsNumberTextTemplate = mcontext.getResources().getQuantityString(R.plurals.restaurant_locations, item.getLocations().size());
        holder.locationsNumberView.setText(String.format(locationsNumberTextTemplate, item.getLocations().size()));

    }


//    public CategoryAdapter(Context context, int resource, List<Category> categories) {
//        mcontext = context;
//        mResource = resource;
//        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        mCategories = categories;
//    }
//
//    @Override
//    public int getCount() {
//        return mCategories.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mCategories.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        View view = convertView == null ? mInflater.inflate(mResource, parent, false) : convertView;
//        holder = new ViewHolder(view);
//        Category item = mCategories.get(position);
//
//        holder.categoryNameView.setText(item.getName());
//        holder.cuisineView.setText(item.getCuisine());
//        String locationsNumberTextTemplate = mcontext.getResources().getQuantityString(R.plurals.restaurant_locations, item.getLocations().size());
//        holder.locationsNumberView.setText(String.format(locationsNumberTextTemplate, item.getLocations().size()));
//
//        return view;
//    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.categoryName)
        TextView categoryNameView;
        @Bind(R.id.categoryCuisine)
        TextView cuisineView;
        @Bind(R.id.locations)
        TextView locationsNumberView;
        Context context;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            LocationFragment locationFragment = new LocationFragment();
            Bundle bundle = new Bundle();
            int position = getAdapterPosition();
            bundle.putSerializable("category", mCategories.get(position));
            locationFragment.setArguments(bundle);
            FragmentManager manager = ((AppCompatActivity) mcontext).getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mainContainer, locationFragment);
            ft.addToBackStack(null);
            ft.commit();

        }


    }


}
