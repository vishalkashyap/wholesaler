package wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.R;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.controller.fragment.OrderPlacementFragment;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Category;
import wholesalerapplication.appcademy.sogeti.com.wholesalerapplication.model.entities.Location;

/**
 * Created by viskashy on 6/13/2016.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private int mResource;
    private List<Location> mLocation;
    private Context mContext;
    private Category mCategory;



    public LocationAdapter(Context context, int resource, List<Location> locations,Category category) {
        mContext = context;
        mResource = resource;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLocation = locations;
        mCategory=category;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_location, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Location item = mLocation.get(position);
       // Bundle bundle = mContext.getArguments();
        holder.cityView.setText(item.getCity());
        holder.addressView.setText(item.getStreetAddress());

    }

    @Override
    public int getItemCount() {
        return mLocation.size();
    }

//    @Override
//    public int getCount() {
//        return mLocation.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mLocation.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        final ViewHolder holder;
//        View view = convertView == null ? mInflater.inflate(mResource, parent, false) : convertView;
//        holder = new ViewHolder(view);
//
//
//        Location item = mLocation.get(position);
//
//        holder.cityView.setText(item.getCity());
//        holder.addressView.setText(item.getStreetAddress());
//
//        return view;
//    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.city)
        TextView cityView;
        @Bind(R.id.address)
        TextView addressView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            OrderPlacementFragment orderPlacementFragment = new OrderPlacementFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("category",mCategory);
            int position = getAdapterPosition();
            bundle.putSerializable("location", mLocation.get(position));
            orderPlacementFragment.setArguments(bundle);
            FragmentManager manager = ((AppCompatActivity) mContext).getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.mainContainer, orderPlacementFragment);
            ft.addToBackStack(null);

            ft.commit();

        }
    }
}
